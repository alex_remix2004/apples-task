<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%apple}}`.
 */
class m210126_075459_create_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apple}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'status' => $this->string(),
            'color' => $this->string(),
            'fall_datetime' => $this->dateTime(),
            'make_datetime' => $this->dateTime(),
            'eat' => $this->integer(),
            'size' => $this->float(),
        ]);

        $this->createIndex(
            'idx-apple-user_id',
            'apple',
            'user_id'
        );

        $this->addForeignKey(
            'fk-apple-user_id',
            'apple',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apple}}');
        $this->dropIndex(
            'idx-apple-user_id',
            'apple'
        );
    }
}
