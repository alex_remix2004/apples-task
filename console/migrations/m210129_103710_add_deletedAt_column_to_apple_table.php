<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%apple}}`.
 */
class m210129_103710_add_deletedAt_column_to_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%apple}}', 'deleted_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%apple}}', 'deleted_at');
    }
}
