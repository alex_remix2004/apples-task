<?php
use yii\base\Application;
use common\models\User;

$container = Yii::$container;

/**
 * Class BaseApplication
 * Здесь добавляем описание компонентов доступных для ВСЕХ
 */
abstract class BaseApplication extends Application
{
}

/**
 * Class WebApplication
 * Здесь только для web
 * @property User $identity
 * @property User $user
 */
class WebApplication extends \yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Здесь только для консоли
 *
 */
class ConsoleApplication extends \yii\console\Application
{
}

/**
 * @return ConsoleApplication|WebApplication|BaseApplication|Application instance
 */
function app()
{
    return Yii::$app;
}

/**
 * @param $value
 */
function dd($value)
{
    print_r('<pre>');
    print_r($value);
    print_r('</pre>');
    die();
}

/**
 * @param null $params
 * @return string
 */
function getAppDomain($params = null)
{
    if (!is_object(app())) {
        return '';
    }
    return app()->params['isLocal'] ? app()->params['localDomain'] . $params : app()->params['appDomain'] . $params;
}

/**
 * Log any variable to selected category
 * @param $var
 * @param string $category
 * @param bool $halt
 */
function logger($var, $category = 'application', $halt = false)
{
    $logger = Yii::getLogger();
    ob_start();
    var_dump($var);
    $data = ob_get_clean();
    $logger->log($data, \yii\log\Logger::LEVEL_INFO, $category);
    !$halt || die();
}

Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('frontendWebroot', getAppDomain() . '/');
Yii::setAlias('backendWebroot', 'http://admin.apples.local/');
