<?php


namespace common\services\button;


use common\interfaces\AppleAccessButtonInterface;
use common\services\AppleQueryService;
use yii\helpers\VarDumper;

/**
 * Class EatButton
 * @package common\services\button
 *
 * @property-read mixed $accessButton
 */
class EatButton extends AbstractAppleButton implements AppleAccessButtonInterface
{
    const CAN_EAT = 'кусать можно';
    const DONT_EAT = 'кусать нельзя';

    private $appleModel;

    public function __construct(AppleQueryService $appleQueryService)
    {
        $this->appleModel = $appleQueryService;
        $this->setStatus();

        parent::__construct();
    }

    public function setStatus()
    {
        switch ($this->appleModel->status) {
            case AppleQueryService::APPLE_ON_TREE:
            case AppleQueryService::APPLE_ROTTEN:
                $this->appleModel->checkStatus = false;
                break;
            default:
                $this->appleModel->checkStatus = true;
                break;
        }

        return $this;
    }

    public function setAccessButton()
    {
        if($this->appleModel->status == self::APPLE_FALLEN && $this->appleModel->eat < self::HUNDRED_PERCENT) {
            $this->appleModel->buttonAccess = self::ENABLED_BUTTON;
        } else {
            $this->appleModel->buttonAccess = self::DISABLED_BUTTON;
        }

        return $this;
    }

    public function setTextButton()
    {
        if ($this->appleModel->checkStatus) {
            $this->appleModel->buttonText = self::CAN_EAT;
        } else {
            $this->appleModel->buttonText = self::DONT_EAT;
        }

        return $this;
    }
}