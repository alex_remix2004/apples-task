<?php
namespace common\services\button;

use common\services\AppleQueryService;

abstract class AbstractAppleButton extends AppleQueryService
{
    const DISABLED_BUTTON = ' disabled';
    const ENABLED_BUTTON = '';
}