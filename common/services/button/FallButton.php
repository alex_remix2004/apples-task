<?php


namespace common\services\button;


use common\interfaces\AppleAccessButtonInterface;
use common\services\AppleQueryService;

class FallButton extends AbstractAppleButton implements AppleAccessButtonInterface
{
    const CAN_FALL = 'уронить';
    const DONT_FALL = 'упало';

    private $appleModel;

    public function __construct(AppleQueryService $appleQueryService)
    {
        $this->appleModel = $appleQueryService;
        $this->setStatus();

        parent::__construct();
    }

    public function setStatus()
    {
        switch ($this->appleModel->status) {
            case AppleQueryService::APPLE_FALLEN:
            case AppleQueryService::APPLE_ROTTEN:
                $this->appleModel->checkStatus = false;
                break;
            default:
                $this->appleModel->checkStatus = true;
                break;
        }

        return $this;
    }

    public function setAccessButton()
    {
        if ($this->appleModel->checkStatus) {
            $this->appleModel->buttonAccess = self::ENABLED_BUTTON;
        } else {
            $this->appleModel->buttonAccess = self::DISABLED_BUTTON;
        }

        return $this;
    }

    public function setTextButton()
    {
        if ($this->appleModel->checkStatus) {
            $this->appleModel->buttonText = self::CAN_FALL;
        } else {
            $this->appleModel->buttonText = self::DONT_FALL;
        }

        return $this;
    }
}