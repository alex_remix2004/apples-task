<?php


namespace common\services;


use common\components\RemoveButtonBehavior;
use common\models\User;
use common\services\button\EatButton;
use common\services\button\FallButton;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * Class AppleQueryService
 * @package common\services
 *
 * @property string $buttonText
 * @property string $buttonAccess
 * @property string $eat
 * @property bool $checkStatus
 *
 * @property-read string $status
 * @property-read string $eatButton
 * @property-read mixed $eatButtonAccess
 * @property-read mixed $eatButtonTitle
 * @property-read string $fallButtonTitle
 * @property-read string $fallButtonAccess
 * @property-read ActiveQuery $user
 */
abstract class AppleQueryService extends ActiveRecord
{
    const APPLE_ON_TREE = 'на_дереве';
    const APPLE_FALLEN = 'упало';
    const APPLE_ROTTEN = 'сгнило';
    const HUNDRED_PERCENT = 100;

    public $buttonText;
    public $buttonAccess;
    public $checkStatus;

    /**
     * @return string[][]
     */
    public function behaviors()
    {
        return [
            ['class' => RemoveButtonBehavior::class],
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::class,
                'softDeleteAttributeValues' => [
                    'deleted_at' => new Expression('NOW()')
                ],
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * AppleQueryService constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return string
     */
    public function getEatButtonTitle()
    {
        (new EatButton($this))->setTextButton();

        return $this->buttonText;
    }

    /**
     * @return string
     */
    public function getEatButtonAccess()
    {
        (new EatButton($this))->setAccessButton();

        return $this->buttonAccess;
    }

    /**
     * @return string
     */
    public function getFallButtonTitle()
    {
        (new FallButton($this))->setTextButton();

        return $this->buttonText;
    }

    /**
     * @return string
     */
    public function getFallButtonAccess()
    {
        (new FallButton($this))->setAccessButton();

        return $this->buttonAccess;
    }
}