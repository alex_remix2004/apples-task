<?php
namespace common\interfaces;

/**
 * Interface ArrayConstantInterface
 */
interface ArrayConstantInterface
{
    public static function getConstantsArray();
}