<?php


namespace common\interfaces;


interface AppleAccessButtonInterface
{
    public function setStatus();
    public function setAccessButton();
}