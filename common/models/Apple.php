<?php

namespace common\models;

use common\services\AppleQueryService;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $status
 * @property string|null $color
 * @property string|null $fall_datetime
 * @property string|null $make_datetime
 * @property int|null $eat
 * @property float|null $size
 */
class Apple extends AppleQueryService
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'eat'], 'integer'],
            [['fall_datetime', 'make_datetime'], 'safe'],
            [['size'], 'number'],
            [['color', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'status' => 'Состояние',
            'color' => 'Цвет',
            'fall_datetime' => 'Дата падения',
            'make_datetime' => 'Дата повяления',
            'eat' => 'Съедено %',
            'size' => 'Размер',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AppleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AppleQuery(get_called_class());
    }
}
