<?php
namespace common\models;

use common\interfaces\ArrayConstantInterface;


/**
 * Class Color
 * @package common\models
 */
class Color implements ArrayConstantInterface
{
    const GREEN = 'green';
    const YELLOW = 'yellow';
    const RED = 'red';

    /**
     * @return string[]
     */
    public static function getConstantsArray()
    {
        return [
            Color::GREEN,
            Color::RED,
            Color::YELLOW,
        ];
    }
}