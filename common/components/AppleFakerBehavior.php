<?php


namespace common\components;


use common\services\AppleQueryService;
use yii\base\Behavior;

/**
 *
 * @property-write mixed $statusAppleFaker
 * @property-write mixed $fallDatetime
 */
class AppleFakerBehavior extends Behavior
{
    const APPLE_ON_TREE = 'на_дереве';

    /**
     * @var $owner
     */
    public $owner;

    private $eatPercent;

    /**
     * @param $faker
     * @return mixed
     */
    public function setStatusAppleFaker($faker = '')
    {
        $this->owner->statusFaker = $faker->randomElement(
            $this->owner->objectStatusFactory()
        );

        return $this->owner->statusFaker;
    }

    /**
     * @param $faker
     * @return mixed|null
     */
    public function setFallDatetime($faker)
    {
        if($this->owner->statusFaker != self::APPLE_ON_TREE) {
            return $faker->dateTimeBetween('-6 hours', '+6 hours')->format('Y-m-d H:i:s');
        }

        return null;
    }

    /**
     * @return int
     */
    public function setEatPercent()
    {
        if ($this->owner->statusFaker == self::APPLE_ON_TREE) {
            $this->eatPercent = 0;
        } else {
            $this->eatPercent = rand(0, AppleQueryService::HUNDRED_PERCENT);
        }

        return $this->eatPercent;
    }

    /**
     * @return float|int
     */
    public function setSizeApple()
    {
        if ($this->eatPercent > 0) {
            return (AppleQueryService::HUNDRED_PERCENT - $this->eatPercent) / AppleQueryService::HUNDRED_PERCENT;
        }

        return 1;
    }

}