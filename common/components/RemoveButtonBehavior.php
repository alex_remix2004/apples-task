<?php


namespace common\components;


use common\services\AppleQueryService;
use yii\base\Behavior;

/**
 * @property bool $statusChecking
 *
 * @property-read string $removeButtonAccess
 *
 * Class RemoveButtonBehavior for access Delete action
 * @package common\components
 */
class RemoveButtonBehavior extends Behavior
{
    const EAT_ALL_PERCENT = 100;

    private $statusChecking;
    public $owner;

    /**
     * @return string
     */
    public function getRemoveButtonAccess()
    {
        if ($this->checkAccess()->checkEatSize()->statusChecking) {
            return ' disabled';
        }
    }

    /**
     * @return $this
     */
    private function checkAccess()
    {
        switch ($this->owner->status) {
            case AppleQueryService::APPLE_ROTTEN:
            case AppleQueryService::APPLE_FALLEN:
                $this->statusChecking = false;
                break;
            default:
                $this->statusChecking = true;
                break;
        }

        return $this;
    }

    /**
     * @return $this
     */
    private function checkEatSize()
    {
        if ($this->owner->status == AppleQueryService::APPLE_FALLEN) {
            if ($this->owner->eat < self::EAT_ALL_PERCENT) {
                $this->statusChecking = true;
            }
        }

        return $this;
    }
}