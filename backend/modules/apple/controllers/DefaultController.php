<?php

namespace backend\modules\apple\controllers;

use backend\modules\apple\classes\AppleFaker;
use backend\modules\apple\classes\AppleFallen;
use backend\modules\apple\classes\AppleModel;
use backend\modules\apple\classes\AppleOnTree;
use common\components\AppleFakerBehavior;
use common\models\Apple;
use common\models\AppleSearch;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `apple` module
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'example' => [
                'class' => AppleFakerBehavior::class,
            ]
        ];
    }

    /**
     * Lists all Apple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppleSearch();
        $dataProvider = $searchModel->search(app()->request->queryParams);

        /*$model = new AppleFaker();
        dd($model->setStatusAppleFaker());*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Generate multiple Apple's models. Before clean old models
     * @param int $times
     * @return Response
     */
    public function actionGenerate($times = 1)
    {
        $appleFaker = new AppleFaker();
        $appleFaker->cleanAppleTable()->run($times);

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionEat($id)
    {
        $model = AppleFallen::findOne($id);

        try {
            $result = $model->tryToEat();
            app()->session->setFlash('success', $result);
        } catch (\Exception $e) {
            app()->session->setFlash('danger',$e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionFall($id)
    {
        $model = AppleOnTree::findOne($id);

        try {
            $result = $model->fallToGround();
            app()->session->setFlash('success', $result);
        } catch (\Exception $e) {
            app()->session->setFlash('danger',$e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Apple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->softDelete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Apple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
