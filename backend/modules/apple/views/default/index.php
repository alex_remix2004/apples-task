<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\AppleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Apples';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apple-index">

    <?= $this->render('//widgets/alert') ?>

    <p>
        <?= Html::a('Generate 10 apples', ['default/generate', 'times' => 10], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Generate 20 apples', ['default/generate', 'times' => 20], ['class' => 'btn btn-info']) ?>
        <?= Html::a('Generate 30 apples', ['default/generate', 'times' => 30], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Generate 40 apples', ['default/generate', 'times' => 40], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Generate 50 apples', ['default/generate', 'times' => 50], ['class' => 'btn btn-danger']) ?>
    </p>

    <p class="bg-info">
        При кусании откусывается, если есть возможность то всегда на 25%
        <br>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user->username;
                },
            ],
            'status',
            'color',
            'fall_datetime',
            'make_datetime',
            'eat',
            'size',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'headerOptions' => ['width' => '200'],
                'template' => '<div class="text-center">{eat} {fall} {delete}</div></div> ',
                'buttons' => [
                    'eat' => function ($url,$model) {
                        return Html::a($model->eatButtonTitle,
                            ['eat', 'id' => $model->id],
                            ['class' => 'btn btn-xs btn-success' . $model->eatButtonAccess]
                        );
                    },
                    'fall' => function ($url,$model) {
                        return Html::a($model->fallButtonTitle,
                            ['fall', 'id' => $model->id],
                            ['class' => 'btn btn-xs btn-warning' . $model->fallButtonAccess]
                        );
                    },
                    'delete' => function ($url, $model, $key) {
                        $options = array_merge([
                            'class' => 'btn btn-xs btn-danger' . $model->removeButtonAccess,
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Вы уверены что хотите удалить '.$model->id.'е яблоко ?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                        return Html::a('Удалить', $url, $options);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
