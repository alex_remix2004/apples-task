<?php


namespace backend\modules\apple\interfaces;

/**
 * Interface AppleStatusInterface
 * @package backend\modules\apple\interfaces
 */
interface AppleStatusInterface
{
    /**
     * @return string
     */
    public function getStatus();
}