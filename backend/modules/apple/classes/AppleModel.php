<?php

namespace backend\modules\apple\classes;

use common\models\Apple;

/**
 * This is the model class for !!! inherited !!! table "apple".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $status
 * @property string|null $color
 * @property string|null $fall_datetime
 * @property string|null $make_datetime
 * @property int|null $eat
 * @property float|null $size
 */
abstract class AppleModel extends Apple
{

}
