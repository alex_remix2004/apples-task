<?php


namespace backend\modules\apple\classes;


use backend\modules\apple\interfaces\AppleStatusInterface;
use yii\db\Expression;

/**
 *
 * @property-read mixed $falling
 */
class AppleOnTree extends AppleModel implements AppleStatusInterface
{
    /**
     * AppleOnTree constructor.
     */
    public function __construct()
    {
        $this->status = static::APPLE_ON_TREE;
        parent::__construct();
    }

    public function rules()
    {
        return [
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function fallToGround()
    {
        return $this->getFalling()->getAnswer();
    }

    /**
     * @return $this
     */
    private function getFalling()
    {
        if ($this->status == self::APPLE_ON_TREE) {
            $this->fall_datetime = new Expression('NOW()');
            $this->status = self::APPLE_FALLEN;
            $this->save();
        }

        return $this;
    }

    /**
     * @return string
     */
    private function getAnswer()
    {
        if ($this->eat == self::APPLE_ON_TREE) {
            return 'Теперь яблоко упало на землю';
        }
    }
}