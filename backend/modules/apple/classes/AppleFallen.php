<?php


namespace backend\modules\apple\classes;


use backend\modules\apple\interfaces\AppleStatusInterface;

/**
 *
 * @property-read string $answer
 */
class AppleFallen extends AppleModel implements AppleStatusInterface
{
    const EAT_PERCENT = 25;
    const RECOUNT_SIZE = 'recount_size';

    /**
     * AppleFallen constructor.
     */
    public function __construct()
    {
        $this->status = static::APPLE_FALLEN;
        parent::__construct();
    }

    public function rules()
    {
        return [
            [['eat'], 'integer'],
        ];
    }

    public function init()
    {
        $this->on(self::RECOUNT_SIZE, [$this, 'recountSizeApple']);

        parent::init();
    }

    /**
     * Event - при откусывании пересчитывает размер яблока
     * @return $this
     */
    public function recountSizeApple()
    {
        $this->size = (self::HUNDRED_PERCENT - $this->eat) / self::HUNDRED_PERCENT;
        $this->save();

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function tryToEat()
    {
        return $this->doEat()->getAnswer();
    }

    /**
     * @return $this
     */
    private function doEat()
    {
        if ($this->eat < self::HUNDRED_PERCENT) {
            if (($this->eat + self::EAT_PERCENT) < self::HUNDRED_PERCENT) {
                $this->eat += self::EAT_PERCENT;
            } else {
                $this->eat = self::HUNDRED_PERCENT;
            }
        }
        $this->save();
        $this->trigger(self::RECOUNT_SIZE);

        return $this;
    }

    /**
     * @return string
     */
    private function getAnswer()
    {
        if ($this->eat == self::HUNDRED_PERCENT) {
            return 'Яблоко съедено полностью';
        } else {
            return 'Теперь яблоко съедено на ' . $this->eat . '%';
        }
    }
}