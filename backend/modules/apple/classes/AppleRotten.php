<?php


namespace backend\modules\apple\classes;


use backend\modules\apple\interfaces\AppleStatusInterface;

class AppleRotten extends AppleModel implements AppleStatusInterface
{
    /**
     * AppleRotten constructor.
     */
    public function __construct()
    {
        $this->status = static::APPLE_ROTTEN;
        parent::__construct();
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}