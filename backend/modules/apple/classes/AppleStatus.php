<?php
namespace backend\modules\apple\classes;

use backend\modules\apple\interfaces\AppleStatusInterface;

/**
 * Class AppleStatus
 * @package backend\modules\apple\classes
 */
class AppleStatus
{
    /**
     * @var AppleStatusInterface
     */
    private $appleProvider;

    /**
     * AppleStatus constructor.
     * @param AppleStatusInterface $appleProvider
     */
    public function __construct(AppleStatusInterface $appleProvider) {
        $this->appleProvider = $appleProvider;
    }
}