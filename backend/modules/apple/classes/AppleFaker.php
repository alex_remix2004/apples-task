<?php


namespace backend\modules\apple\classes;


use common\components\AppleFakerBehavior;
use common\models\Color;
use Faker\Factory;
use yii\base\Model;
use yii\db\Exception;

/**
 * Class AppleFaker
 * @package backend\modules\apple\classes
 *
 * @property string $statusFaker
 *
 * @property-read array $data
 */
class AppleFaker extends Model
{
    /**
     * limit for the generating models
     */
    const MAX_RECORDS = 100;

    public $statusFaker;

    public function behaviors()
    {
        return [
            ['class' => AppleFakerBehavior::class],
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = [];
        $faker = Factory::create();

        $data[] = [
            1,
            $this->setStatusAppleFaker($faker),
            $faker->randomElement(
                Color::getConstantsArray()
            ),
            $this->setFallDatetime($faker),
            $faker->dateTimeBetween('-6 hours', '+6 hours')->format('Y-m-d H:i:s'),
            $this->setEatPercent(),
            $this->setSizeApple()
        ];

        return $data;
    }

    /**
     * @return array
     */
    public function objectStatusFactory()
    {
        return [
            (new AppleFallen())->getStatus(),
            (new AppleRotten())->getStatus(),
            (new AppleOnTree())->getStatus(),
        ];
    }


    /**
     * @throws Exception
     */
    private function commandFaker()
    {
        app()->db
            ->createCommand()
            ->batchInsert('apple',
                [
                    'user_id',
                    'status',
                    'color',
                    'fall_datetime',
                    'make_datetime',
                    'eat',
                    'size',
                ],
                $this->getData())
            ->execute();
    }

    /**
     * @param $times
     */
    private function countChecker($times)
    {
        $times = $times > self::MAX_RECORDS ? self::MAX_RECORDS : $times;
        for ($i=0; $i < $times; $i++) {
            try {
                $this->commandFaker();
            } catch (Exception $e) {
                \Yii::error($e);
            }
        }
    }

    /**
     * @param $times (default = 1)
     */
    public function run($times)
    {
        $this->countChecker($times);
    }

    public function cleanAppleTable()
    {
        AppleModel::deleteAll('user_id = ' . app()->user->id);

        return $this;
    }
}