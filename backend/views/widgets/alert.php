<?= \common\widgets\Alert::widget() ?>
<?php
$js=<<< JS
     $(".alert").animate({opacity: 1.0}, 4000).fadeOut("slow");
JS;

$this->registerJs($js, yii\web\View::POS_READY);
?>
