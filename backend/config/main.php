<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'apple' => [
            'class' => \backend\modules\apple\Module::class,
        ],
    ],
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'pattern' => '<controller>/<action>',
                    'route' => '<controller>/<action>',
                ],
                [
                    'pattern' => '<controller>/<action>/<id:\id+>',
                    'route' => '<controller>/<action>',
                ],
                '<module:apple>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:apple>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                [
                    'pattern'=>'',
                    'route'=>'/',
                ],
            ],
        ],
    ],
    'params' => $params,
];
